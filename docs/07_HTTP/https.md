# HTTPS protocol

HTTPS is the "secure" version of HTTP. It is implemented as normal HTTP encapsulation in an encrypted tunnel.

[Howotgeek](https://www.howtogeek.com/181767/htg-explains-what-is-https-and-why-should-i-care/) has some more words on it.

There are two fundamental types of encrypting data: symmetric and asymmetric. Both are used extensively. Their cousin, hashing, is important also. See e.g. [here](https://www.cryptomathic.com/news-events/blog/differences-between-hash-functions-symmetric-asymmetric-algorithms) and [this](https://www.khanacademy.org/computing/computers-and-internet/xcae6f4a7ff015e7d:online-data-security/xcae6f4a7ff015e7d:data-encryption-techniques/a/the-need-for-encryption) from Kahn academy.

Note that, as with all security, the concept of "secure" is context dependant and never 100%. There are a lot of design, coding and configuration issues related to all technical implementations of "secure".

Just like most other plain text protocol that need protection, we use TLS for encryption. This is true for [ftp](https://en.wikipedia.org/wiki/FTPS), [smtp](https://en.wikipedia.org/wiki/SMTPS), imap, pop3, [DNS](https://en.wikipedia.org/wiki/DNS_over_TLS) and others.

There are some technical differences between expecting the connection to be encryption (like HTTPS) and starting the encryption after the initial plaintext connection (this is usually done using some sort of STARTTLS command). See e.g. [this introduction](https://blog.mailtrap.io/starttls-ssl-tls/)


## Symmetric encryption (30 min)

In this exercise, we will aim at getting an understanding of symmetric encryption. There are a lot of technical information that is too deep, so glancing most parts is recommended.

1. Read [this](https://www.cryptomathic.com/news-events/blog/symmetric-key-encryption-why-where-and-how-its-used-in-banking) and [this](https://academy.binance.com/en/articles/what-is-symmetric-key-cryptography) as starting points.

2. Research what is symmetric encryption.

    "AES" is massively important here.

4. Write the three best resources in a document and add 2-3 sentences describing why they are good.

3. In your own words, write 10 lines about what symmetric encryption is and what it is used for.

## Asymmetric encryption (30 min)

1. Read [this](https://www.cloudflare.com/learning/ssl/what-is-asymmetric-encryption/) and [this](https://ssd.eff.org/en/module/deep-dive-end-end-encryption-how-do-public-key-encryption-systems-work) from the EFF as starting points.

2. Research what is asymmetric encryption

    "Public keys" and "private keys" are fundamental

4. Write the three best resources in a document and add 2-3 sentences describing why they are good.

3. In your own words, write 10 lines about what asymmetric encryption is and what it is used for.

## HTTPS and certificates (45 min)

1. Two videos about HTTPS and certificates: [here](https://www.youtube.com/watch?v=T4Df5_cojAs) and [here](https://www.youtube.com/watch?v=LRMBZhdFjDI)

    Using the knowledge from the previous exercise, your should be able to understand most of the concepts introduced.

2. Start your browser and go to `https://google.com`

    In Kali or on our host, at your leasure.

3. Click on the padlock next to the URL, and select to see the certificate for the site.

    For chrome there are some options presented [here](https://dzone.com/articles/how-to-view-ssl-certificate-in-chrome) and similar for [firefox](https://support.mozilla.org/en-US/kb/secure-website-certificate)

4. Read the certificate and look for

    * who issued it?
    * issued to which domains?
    * validity? and for which period?
    * anything else?

5. Do the same for `gitlab.com`

6. Do the same for `moozer.gitlab.io`

7. Write a short summary where you compare the three sites.

## Fetching HTTPS on the command line (15 min)

1. Start a terminal in Kali

2. Run `curl https://www.google.com`

    Explain what is seen.

3. Run `curl -v https://www.google.com`

    Explain what is seen.

7. Make a short writeup of your findings.

## HTTPS and wireshark (20 min)

Previously, this kind of exercise was easier, but since the introduction of TLS 1.3, simply adding the correct private key to wireshark and decrypt TLS traffic is not possible anymore.

Good for security, bad for exercises.

1. In Kali, start wireshark

2. Connect to `https://google.com`

3. Refind the packets in Wireshark

    Either by DNS+ipadress, or limiting to a specific tcp stream.

4. Look at the "Client hello" and "Server hello" packets

    Using the detailed view in wireshark, do you recognize anything from the previous exercises.

5. Write a short summary of your findings.

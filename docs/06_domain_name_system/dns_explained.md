# DNS explained

The domain name system is very fundamental to how we use the internet today. In principle, it is not needed, but for all practical purposes, we want to use names of servers and domains instead of using IP adresses.

An explanation of domains is availabe from [ICANN](https://www.icann.org/en/system/files/files/domain-names-beginners-guide-06dec10-en.pdf)

## The DNS hierachy

The basic idea of DNS is explained in [this video](https://www.youtube.com/watch?v=mpQZVYPuDGU) by powercert.

Key takeaways:

* DNS translates from domain names to IP adresses
* DNS is distributed
* DNS uses a tree-like structure
* All domains have an associated nameserver
* DNS uses "records", e.g. "A" for ipv4 host address

## DNS records (45 min)

1. Read up on the different records, e.g. [here](https://www.techopedia.com/2/28806/internet/12-dns-records-explained= or [here](https://www.cloudflare.com/learning/dns/dns-records/), and perhaps watch a [video](https://www.youtube.com/watch?v=6uEwzkfViSM)
2. Explain in your own words the use and/or the purpose of
    * "A" records
    * "CNAME" records
    * "PTR" records
    * "MX" records
    * "NS" records
3. Find the description of the records in the RFCs.
    DNS is formally  defined in [RFC 1034](https://tools.ietf.org/html/rfc1034) and [RFC 1035](https://tools.ietf.org/html/rfc1035).
    Don't read them in detail - they are reference material.

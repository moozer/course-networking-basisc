# Domain name system

## Goals

The goal for this part is for the student to

* explain how DNS works
* test and use common DNS tools

There are two subparts:

* [DNS explained](dns_explained.md)
* [DNS tools](dns_tools.md)

The first part is an introduction to the DNS system and the second part is about the tools used.

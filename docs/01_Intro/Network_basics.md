# Network basics

Introducing the

<iframe width="560" height="315" src="https://www.youtube.com/embed/0kWRYPcAwYI" frameborder="0" allowfullscreen></iframe>

In the video some key concepts are introduced

* `OSI Layers`. This topic will be revisited later
* `IP addresses`. Logical addresses assigned to a network interface
* `Netmask`: The value used to determine if an IP addresses is on the local subnet
* `MAC addresses`. Hardware addresses
* `Subnet`: Usually a local network connected by one or more switches.
* `Network`: Usually a collection of `subnets` connected by one or more `routers`.
* `Switch`: A layer 2 device working with `MAC addresses`.
* `Router`: A layer 3 device working with `IP addresses`

## Find online resources

Use google or similar search engine to find information on the topics above

1. Pick a topic
2. Find 1-2 `good` sources describing the topic
3. Write 2-3 sentences about what is good about the chosen resource
3. Redo 1-3 3 times
4. Compare notes with the person next to you
5. Put the links and sentences in a shared document

## Exercise: Draw your home network (1h)

Use the diagramming style presented in the video. This is a first attempt at this, so accept ambiguities and that your notation is not always correct.

This is a pen&paper exercise.

1. Choose a simple network
    This may be your home network og similar

2. Draw a physical cabling diagram including all hardware.

    This will show e.g. wireless and ADSL modems. Include an `internet` cloud.

3. Draw a logical network diagram using subnets.

    This focusses on IP addresses, but not layers 1+2. ie. ony thing with an IP address.

4. Swap diagrams with a co-student.

    * Are the diagrams understandable?
    * Are they complete? any obvious omissions?

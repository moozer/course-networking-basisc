# Virtualization

Virtualization is everywhere these days, and in this course, you will be presented with virtual machines to use in exercises.

<iframe width="560" height="315" src="https://www.youtube.com/embed/R6fEVLtcJXg" frameborder="0" allowfullscreen></iframe>


## Your words on virtualization

Using a variation on the Feynman technique, work with the topic of "Virtualization".

1. Write ½ page about "Virtualization"
2. Highlight 5 places in your text where you feel the weakest
3. Research the 5 topics

    Make a list of the 5 topics, add relevant links and 1-2 sentences of clarifying comments.

4. (Optional) Reiterate 1-3

    Time will probably not allow for multiple iterations.

5. Put the topics, links and comments in a shared document.


## Install Vmware workstation

Install VM ware workstation. This will be used for exercises later in the course.

1. Download and install the evaluation version. See [here](https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html)


## Run Kali Live

1. Download the Kali linux .iso file from [here](https://www.kali.org/downloads/)

    Unless you have non-standard hardware, you want the "Kali linux 64-Bit (Live)"

2. (While downloading) Research what "live" means in this context.

    Make notes of relevant links.

3. Make a drawing of what virtual hardwar that is needed to use a VM and access the network.

    To make nice looking computer drawn diagram, a suggestions is to use yEd. A video about it [here](https://www.youtube.com/watch?v=BdKuuEfviEo)

2. Create a new virtual machine and use the iso file as "CDROM".

    See e.g. [this question](https://communities.vmware.com/thread/597839) and [this](https://petri.com/use-iso-image-files-vmware) about .iso files.

4. Start a terminal inside the Kali box, and post a screenshot of `traceroute 8.8.8.8`

    If you know, please explain what is shown.

A video going through this exercise is available [here](https://www.youtube.com/watch?v=46jSL0ujGTc)

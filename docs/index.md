# Networking

The aim for this site is to be a collection of resources for students that want to learn basic networking.

The goal is for the student to have operational understanding and skills in relation to networking. Theory will be minimal, and hands-on is emphasized.


## Introduction

Getting to know the basic concepts and tools.

Concepts: Switches, routers, IP, frame, ethernet, subnet, netmask, ISO

Skills: use `vmware wrkstation` (install, start VM), draw network diagrams

Go [here](01_Intro/index.md)

## OSI model and packets

Going through the OSI model, and use Wireshark to see the data on the wire.

Concepts: OSI model, layers, packets, sniffing traffic

Skills: use `wireshark`, use `ping`

Go [here](02_OSI/index.md)

## Layer 3: IP, routers and routing

IP adresses are used for routing, and to get beyond the local subnet.

Concepts: IP addresses, gateways, NAT, routing

Skills: Simple linux CLI, draw network diagrams, use `wireshark`

Go [here](03_IP/index.md)

## Virtualized networks

Using VMware workstation or similar virtualization software gives options as to how a virtual machine is connected to both physical and virtual networks.

Concepts: virtual networks, virtual machines, routing, IP, NAT, bridging

Skills: Use `VMware workstation` (setup networks, import .ova), troubleshoot, use `wireshark`

Go [here](04_virtual_networks/index.md)


## More routing

Routing using virtual machines in vmware workstation

Concepts: IP, virtual machines, network layout

Skills: Use `VMware workstation` (Importing VMs, creating vmnets, connecting VMs to vmnets,), use `wireshark`, draw diagrams

Go [here](05_more_routing)

## Domain name system

DNS is the mechanism that translates from domain names to IP adresses.

Concepts: IP, domain names, DNS protocol (RFC1034 and RFC1035), DNS record, DNS server infrastructure

Skills: Use `dig`, use `wireshark`, simple Linux CLI

Go [here](06_domain_name_system)

## HTTP and HTTPS

HTTP (and HTTPS) is the protocol users see the most. It is the one used to "surf" the web. It is increasingly used for backend systems also.

Concepts: IP/domain names, HTTP protocol ([RFC7231](https://tools.ietf.org/html/rfc7231)), HTTPS, web server and clients, symmetric and asymmetric encryption, certificates, TLS, AES

Skills: Use `wget`, use `wireshark`, simple Linux CLI

Go [here](07_HTTP)

## Designing networks

Designing a network from scratch is a good way of training the routing and physical layer understanding.

Keywords: IP, subnets, routers, network hardware, tunnels

Skills: Design networks/topologies, troubleshoot networks errors, draw diagrams

Go [here](08_network_layout)


## Internet

The is a build-in structure of the internet.

Keywords: Public and private IPs, autonomous systems (AS), BGP, IANA, RIPE, regional internet registries (RIR), ISP, looking glass, prefix/network address, routes

Skills: Use `traceroute`, use `whois`

Go [here](09_Internet)

## Consolidation

In this part, we wrap up the material covered and ties it together.

Keywords: IP, gateway, web proxy, dns server, virtualization, virtual networks, DNS traffic, HTTP/S traffic

Skills: Import virtual machines, troubleshooting, linux cli, static ip, inspection in browser, use `wireshark`, use `dig`, simple linux CLI, draw network diagrams

Go [here](10_Consolidation)

## Bonus

We have included some bonus stuff that may of relevance.

Go [here](Bonus)

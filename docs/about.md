# About

This is made to in order for my materials to be publicly available for anyone. It is intended to be 5 ECTS points /approx. 130 hours) worth of work for the students, with focus being on getting an operational understanding of the material.

This "public" concept also means that it is easy to links to.

The entire site is tracked on [gitlab](https://gitlab.com/moozer/course-networking-basisc), and mostly made by [moozer](https://gitlab.com/moozer).

Contact either using [issues/MR on the project](https://gitlab.com/moozer/course-networking-basisc/-/issues), [tweet me](https://twitter.com/bigmoozer) or [tooting](https://mastodon.social/@moozer).

Issues might be request for content on a specific topic.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

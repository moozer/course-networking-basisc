# Host only

Sometime we do not want the VMs to be able to connect to the internet. This might be internal servers or if servers has multiple interfaces.


## Host only in vmware

1. Make a network diagram showing the VM connected using host only
1. modify the network setting for your kali linux to bridging
3. Spin up kali
4. Document that you are doing host only.

## Your own router - wip

1. Download the router .ova file
2. Connect the interfaces to bridge and the other to host-only
3. Ensure the the kali is on host-only
4. Draw the network diagram for the above network
5. Spin up both VMs
6. Modify Kali so it uses the router as gateway
7. Document that it works.

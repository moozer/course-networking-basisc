# Designing networks

## Goals

The goal for this part is for the student to

* design simple networks
* document simple networks
* explain how to test network

Simple networks in this context is SOHO networks.

Since hardware is mostly non-virtual, this part is theoretical.

There are three parts:

* [Hardware](hardware.md)
* [Designing](designing.md)

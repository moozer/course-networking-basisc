# Hardware

In order to build networks, there is a need to know what hardware is available.

This will build a lot on the OSI model. A [previous topic](docs/02_OSI) of this course.

Most devices are "multi function" devices, as opposed to being pure switches or routers.

## An example: Home router

The router supplied by an ISP could be called a cable router with builtin wifi. A 2020 top 10 review [from lifewire](https://www.lifewire.com/best-cable-modem-router-combos-to-buy-4082541) gives examples of actual hardware.

There are multifunction device with (at least) the following functions:

* Modem: Converts the cable signal to and from ethernet (IEEE 802.3) and wifi (IEEE 802.11).
* Switch: Multiple ethernet connectors
* Wifi: 2.4GHz and 5GHz wifi radio hardware
* Routing: Since ip address space is different on the inside and on the outside, the device most be able to do routing.
* Statefull firewall/NAT: The usual implementation of NAT is to have it as a part of a statefull firewall. This usually includes support for [port forwards](https://en.wikipedia.org/wiki/Port_forwarding) or [DMZ host](https://en.wikipedia.org/wiki/DMZ_%28computing%29#DMZ_host).
* Webserver/SSH: To configure the device, the norm is to have a webinterface on the inside interface. SSH is also common for text mode acces, especially on "business" models.

The list is more rigid than reality. Looking at the inside of a router, there will be integrated circuitry that, themselves, are multifunction.

## Network hardware and servers (1h)

There are many different devices available. The following is an incomplete list.

* Modems
* Router
* Unmanaged switch
* Managed switch
* Access point
* Wireless controller (sometimes hardware, sometimes a server)
* Web proxy (sometimes hardware, sometimes a server)
* Load balancer (sometimes hardware, sometimes a server)
* Firewall
* Wireless repeater
* Wireless bridge
* VPN concentrator

Do the following

1. Research each element on the above list, and answer the following

    * What do they do?
    * On which OSI layer do they operate?

2. Make a document with the above documentation and include relevant links

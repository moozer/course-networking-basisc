# Designing networks

The way a network is layed out is called the topology. There are some recurring patterns, like multiple subnets, placements of firewall, and other. This will be dependent on the context, devices, security and more

Having established the available hardware, some auxilary techniques must be introduced

* Segmentation
* VLANs
* DMZ
* Port forwards
* Tunnels
* VPN
* Jump hosts, proxies and such

## Segmentation

This is something that should be done much more in most networks.

To its core, it means to create more subnets, and have appropriate security to separate them.

Different groupings of devices must reside in their own subnet of the network. Groupings could be:

* User devices
* BYOD or guest wireless devices
* Infrastucture servers and devices
* Internal servers: AD, fileservers, intranet web, VoIP
* Externally accessible servers: webservers, mail, dns

This has massive security implications, since malcious agents often gets foothold on a network either on a external facing server or through a user PC. Proper segmentation will delay the attacker and make logging possible to detect the intrusion.

Some references:

* Segmentation of network is considered best practice e.g. in [CIS controls #14](https://www.cisecurity.org/controls/controlled-access-based-on-the-need-to-know/)
* More on segmentation from [cisco](https://www.cisco.com/c/en/us/products/security/what-is-network-segmentation.html) and [palo alto](https://www.paloaltonetworks.com/cyberpedia/what-is-network-segmentation)

## VLANs

When segmenting a network, a need arizes for the same cables to hold more subnets.

Having multiple subnets overlapping will result in issues with broadcast and multicast traffic. It has the derived effect of devices that devices separated on the network layer may contact each other on the datalink layer. This design has been deprecated for a long time and now all managed switches support 802.1q VLAN tagging.

The practical implementation is to have "trunk" and "access" port configured on the switches.

* A "trunk" port sends an receives multiple subnets on the same cable, in separate LANs tagged using 802.1q to destinuish them.
* An "access" port is a port on the switch, where the internal configuration of the switch send one untagged subnet on the port. This way a switch can have multiple separated subnets connected at the same time.

Powercert has a [video](https://www.youtube.com/watch?v=jC6MJTh9fRE) describing the above.

Practically speaking the VLAN tag is a extra part of the packets as shown on [wikipedia](https://en.wikipedia.org/wiki/IEEE_802.1Q#Frame_format).

## DMZ

DMS - demilitatized zone - is commonly used for internet facing services. This is considered a fairly insecure zone, and the servers will have to be hardened since they are accesible directly from the internet.

References:

* Fra [D-link](https://eu.dlink.com/dk/da/support/faq/firewall/what-is-dmz)
* Fra [Juniper](https://www.juniper.net/documentation/en_US/junos-space17.2/topics/concept/junos-space-zone-set-understanding.html)
* Fra [Barracuda](https://www.barracuda.com/glossary/dmz-network)

## Port forwards

This is a concept that is applicable only to routers doing NAT. A port forwards maps an external UDP or TCP port on the router to an internal ip address and port.

A note on semantics: The port forward mentioned above is "destination nat", not "source nat", which is the `NAT` as used on this site.

Referencer:

* How to setup a [generic guide to multiple brands](https://www.noip.com/support/knowledgebase/general-port-forwarding-guide/)
* [Wikipedia](https://en.wikipedia.org/wiki/Port_forwarding)

## VPN

VPN - virtual private networking - is a way of making an encrypted tunnel to a network.

There are two primary setups:

* Site-to-site: If a company has multiple locations, an encrypted tunnel may be set up, so it becomes transparent for the users (and servers) that their data is transmitted over the internet.
* Client-server: This is the scenario where one device connects to a network, e.g. an employee working from home or travelling. In practical terms, the remote device gets an ip address in a specific subnet and is then treated as a local non-internet device.

There are also VPN services online that is designed to protect your private IP address. This is a variation on the client-server setup shown above. There are a lot of vendors selling this service - you must consider your thread source before using this option.

## Jump hosts, proxies and such

Having segmented the network, there will be different trust zones, especially the internet will need special handling.

There are application layer devices available to act a middle-man to filter and audit the traffic going in and out.

* Load balancer: Sometimes application specific, sometimes just TCP. It takes the connection from e.g. the internet and forwards it to internal servers based on some algorithm to spread out the load of e.g. rendering web pages to multiple internal servers.
* Web proxy: An HTTP/S proxy that a user device uses to access web services on the internet.
* Reverse proxy: An HTTP/S proxy that is designed to be connected to from the internet and forwards the HTTP requests to internal host(s). This may function as both fail-over and load-balancing.
* Jump hosts and bastion hosts: A common server set up to receive SSH connections and then "jump" further into the network from that host. Managing servers and infrastructure devices has security implicaitons, and should be limited. Jump hosts are a way of giving remote administrators or automated systems access to protected internal devices.


# SOHO network (30 min)

You are to design a network with the following components

* 3 user laptops on wireless
* 1 user desktop on cable
* 1 printer
* Internet connection using fiber
* NAS (networked attached storage) for internal file sharing.

Do the following

1. Decide on subnets and ip ranges
2. Draw the logical network diagram
3. Draw a physical network diagram
4. Describe how to debug if one of the laptops cannot access the internet
5. Describe how to debug if the desktop cannot access files on the NAS

# Not so SOHO network (45 min)

You are to design a network with the following components

* 30 user laptops on wireless
* 15 user desktop on cable
* 3 printers
* Internet connection using fiber
* NAS (networked attached storage) for internal file sharing
* VPN access for laptops when remote
* 2x Windows server
* Webserver exposed to the internet

Do the following

1. Decide on subnets and ip ranges
2. Draw the logical network diagram
3. Draw a physical network diagram
4. Describe how to debug if one of the laptops cannot access the internet
5. Describe how to debug if the desktop cannot access files on the NAS
6. Describe how to debug if a laptop cannot access the files using VPN

# IP adresses

In most networks, we have two logical adresses that are used: IP version 4 addresses and IP version 6.

Adoption for IP version 6 is currently poor, and will not be addresses here. It is defined in [RFC 8200](https://tools.ietf.org/html/rfc8200) and [wikipedia](https://en.wikipedia.org/wiki/IPv6) includes a lot of the relevant terms as onset for further reading.

An overview and som history is available from [ICANN](https://www.icann.org/en/system/files/files/ip-addresses-beginners-guide-04mar11-en.pdf)

## IPv4

[Video from NexGenT](https://www.youtube.com/watch?v=XQ3T14SIlV4) about ip v4 addresses and subnets. There is also [an introductory video](https://www.youtube.com/watch?v=ddM9AcreVqY) related to bits and how to read ipv4 addresses.

* `IPv4 address`, e.g. `192.168.0.10` is the address of a network connected device.
* `Netmask`, e.g `255.255.255.0` is a measure of how large a given subnet is, as in how IP addresses are allowed on a subnet. The combination above may be written as `192.168.0.10/24` in CIDR notation.

An explanation of netmasks may be found at [fs.com](https://community.fs.com/blog/know-ip-address-and-subnet-mask.html)

Read about CIDR notation at [wikipedia](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing) or best current practice in [RFC 4632](https://tools.ietf.org/html/rfc4632)

* `Gateway address` is the common name of the address to use to leave your local subnet. This is normally the inside interface of a router.

There are also the `broadcast` address and `multicast` addresses and more, which we will revisit.

Ipv4 operates with the concept of `private` and `public` ip addresses. It refers to whether or not the addresses are available on the internet. It is defined in RFC 1918.

## Subnet sizes

Use an online calculator to calculate the size of different sized networks.

An online calculatr may be found at [calculator.net](https://www.calculator.net/ip-subnet-calculator.html?cclass=any&csubnet=24&cip=192.168.0.1&ctype=ipv4&printit=0&x=91&y=28)

1. Default home router network: `192.168.0.0/24`
2. RFC 1918 private ip addresses.
3. Cloudflarenet: `172.64.0.0/13`
4. Using `ip a` on linux or `ipconfig` on windows, locate the ip address and netmask of your workstation.
5. Put results in a readable fashion in a shared doument.

## Dynamic IP

The IP address on an interface will be dependent on which network it is connected to. Default for most devices is to use DHCP, which is a way for the network to supply addresses to a device.

[Video about DHCP](https://www.youtube.com/watch?v=e6-TaH5bkjo) from powercert.

## Sniffing DHCP traffic

1. Spin up the Kali linux VM
1. Start wireshark and start collecting packet on the main interface.

2. Renew the DHCP lease using `sudo dhclient -v eth0`
3. Refind the packets in wireshark
4. Read the packet

    What is send from Kali and what is received?
    Which ip addresses do you see?

5. Make a list of all IP adresses and other relevant information, add explanatory notes and put the result in a shared document.

## Static IP address

As opposed to a dynamic address where the network supplies the ip information, a static address may be set.

This address must be compatible with the network the device is connected to.

Ip addresses may be set in an ephemeral manner, where the information is lost after a reboot, or in a persistent manner where the configuration file are updated.

Using a Kali live, the latter is not an option.

There are multiple ways of setting the ip address. One way is through network manager and another is using the comand line.

## Exercise: Setting a static IP address

1. Using the values from previous exercises, select an IP address and subnet to use.

1. Spin up the Kali linux VM and open a terminal.

2. Release the dhcp address `sudo dhclient -r eth0`

3. Set the address using `ip a`

    See e.g. [here](https://www.tecmint.com/ip-command-examples/) for an example

1. Start wireshark and start collecting packet on the interface.

4. `ping` the gateway address

5. Refind the packets and describe what is seen.

6. `ping 8.8.8.8`

    Refind the packets, if applicable.
    Describe what is seen.
    Are you able to ping an address on the internet?

7. Put results in a shared document.

# IP

## Goals

The goal for this part is for the student to

* have a working understanding of ip (version 4) adresses, netmask and subnets.

There are two subparts:

* [IP addresses](ip_addresses.md)
* [Routing](routing.md)

The first part is an introduction to IPv4 addresses and netmask. The second part is about routing, ie. how the IP address is used to send packets in a network.

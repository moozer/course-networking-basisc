# Bonus

This is a collection of "nice to have" and other supporting resources.

* [Setting IP adresses in OpenBSD](ob_ip_update.md) in the provided routes
* The ansible script used to build the squidns server from [the _consolidation_ part](../10_Consolidation/setting_up) is on [gitlab](https://gitlab.com/moozer/ansible-squidns)

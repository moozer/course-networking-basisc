# OSI model

## Goals

The goal for this part is for the student to

* have a working understanding of the OSI model layers
* be able to use wireshark and tcpdump to analyse network trafic

There are two subparts:

* [OSI model](osi_model.md)
* [Looking at network traffic](nw_traffic.md)

The first part is to get you started with OSI models. The second is to use Kali linux to clarify the OSI models.

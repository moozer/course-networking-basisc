# Network traffic

We will be going through basic networking and how to extract network traffic.

##  Network traffic sniffing (30 min)

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/b_XQ7Xg1Jek" frameborder="0" allowfullscreen></iframe>

There are three ways of listening in on communications

1. List the three ways
2. Describe in your own words each method

    Include at which layer each method operates

3. Put the descriptions in a shared file

## Network interfaces in linux (30 min)

1. Spin up a VM with Kali
2. Use `ip address` to see the IP and MAC addresses
3. Explain what the command does and what is shown
3. Use `ip route`
3. Explain what the command does and what is shown
4. Put the command output and the explanation in a shared file

## Wireshark in kali (30 min)

Minimal wireshark kickstart video:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embedHat1zFITqw4" frameborder="0" allowfullscreen></iframe>

Another [video about wireshark](https://www.youtube.com/watch?v=YBB2CBim_Ys) (start at 5:00)
Some written introduction from [Redhat](https://www.redhat.com/sysadmin/introduction-wireshark) and [medium](https://medium.com/@koayyongcett/a-brief-introduction-about-wireshark-and-some-basic-functions-9521fc6b6681). Filter reference is [here](https://www.wireshark.org/docs/dfref/)

1. Boot up Kali
2. start wireshark, and listen on interface eth0
4. in a terminal, ping 8.8.8.8
5. refind the packets in wireshark
6. select the echo ping request packet
7. what are the MAC adresses and ip adresses?

    Notice the layered info, starting with  "frame", "ethernet" and "Internet protocol"

8. Explain what each of the 4 values means and which devices they refer to
8. Ping 1.1.1.1
5. Refind the packets in wireshark
6. select the echo ping request packet
7. What are the MAC adresses and ip adresses?
8. Explain what each of the 4 values means and which devices they refer to
10. Compare values for the two pings and explain similarities/differences
11. Put addresses and descriptions in a shared document

    This is a .md file, so you could [add images](https://www.markdownguide.org/basic-syntax/#images-1)

There is a video convering this exercise on [youtube](https://www.youtube.com/watch?v=DdJZ7mCF8s8)

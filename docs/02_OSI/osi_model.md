# OSI model

Get introduced to the osi model

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/e7p4N0EeB9c" frameborder="0" allowfullscreen></iframe>

The OSI model has 7 layers

1. Physical
2. Data link
3. Network
4. Transport
5. Session
6. Presentation
7. Application

A video discussing layer 1

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Lp4GMzdvvGE" frameborder="0" allowfullscreen></iframe>

## Exercise: Find online resources (1h)

There are many resources online about the OSI model.

1. Search the web, and find 2 articles and 2 videos.
3. Write 2-3 sentences about what is good about the chosen resource

    Are the sources "authoritative"?

4. Compare notes with the person next to you
5. Put the links and sentences in a shared document

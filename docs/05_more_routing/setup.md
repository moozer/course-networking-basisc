# Setup

In order to align the setups, some adaption of vmware workstation is needed.

## Creating new subnets (30 min)

This is done in "Virtual network editor" in vmware workstation. See e.g. [sysnettechsolutions](https://www.sysnettechsolutions.com/en/vmware-workstation-custom-specific-virtual-network/) for a guide and images.

1. Create a new subnet called `vmnet111`, with the following parameters

    * Host-only network
    * No DHCP service
    * Subnet IP: 192.168.111.0, netmask: 255.255.255.0
    * Connect a host virtual adapter
    * If you are not allowed to call it `vmnet111`, call it something else, and make a note of it.

1. Create a new subnet called `vmnet112`, with the following parameters

    * Host-only network
    * No DHCP service
    * Subnet IP: 192.168.112.0, netmask: 255.255.255.0
    * Connect a host virtual adapter
    * If you are not allowed to call it `vmnet112`, call it something else, and make a not of it.

3. Verify that `vmnet8` is a "NAT" network with DHCP enables.

    IP range is not important.



## Importing router (1h)

In vmware, virtual appliances are usually bundled as .ova files. The official import docs are [here](https://docs.vmware.com/en/VMware-Workstation-Pro/15.0/com.vmware.ws.using.doc/GUID-DDCBE9C0-0EC9-4D09-8042-18436DA62F7A.html) and the same just with images from [informatiweb](https://us.informatiweb-pro.net/virtualization/vmware/vmware-workstation-15-export-and-import-vms--2.html).

The network we are building is shown below

![router-nat network](router_nat.png)

1. Download the ova file from [here](https://drive.google.com/file/d/1tS70JCYqOrzIWS_TZxdB1X7Hyet0v-dH/view?usp=sharing)

    Note that .ova files a usually large, so downloading in wireless might take a long time, especially is others a saturating the connection at the same time. Consider download using cable or share using USB.

2. Import .ova file

3. Check the settings for the new VM

    * Network adapter: connect to `vmnet8`
    * Network adapter 2: connect to `vmnet111`
    * Network adapter 3: connect to `vmnet112`

3. Start the new vm

4. Connect the virtual Kali machine to `vmnet111` (aka. `USR_LAN`)

5. Check ip adresses and routing, ad verify you have internet access.

6. Put the info in a shared doc.


## An extra VM (30 min)

1. Reuse the setup from above

2. Create a new VM

    This could be a second Kali live

3. Connect it to `vmnet112` (aka. `SRV_LAN`)

4. Ping one kali from the other, and use wireshark to document that no NAT'ing occurs between the two VMs.

5. Put documentation in a shared doc.

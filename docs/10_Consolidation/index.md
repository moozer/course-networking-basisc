# The internet

## Goals

The goal for this part is for the student to

* use networking knowledge to troubleshoot
* configure networks
* trace HTTP and DNs using network tools

There are three parts:

* [Setup](setting_up.md)
* [DNS and HTTP](dns_http.md)

## DNS and HTTP

The server is configured to be a caching dns server and a web proxy

The goal is to use both.

## Using the DNS server

1. Set kali to use the server as DNS server

    This is a different DNS server than the one given by the DHCP server.

    In network manager, there is a flag for specifying your own DNS server instead. See e.g. this [stackoverflow](https://askubuntu.com/questions/2321/what-is-the-proper-way-to-change-the-dns-ip) question.

2. Use `wireshark` to check that kali acutally uses the new server as DNS server.

2. Oh no! It is not working even with the correct DNS server in Kali.

    What DNS trafic is the DNS server sending and receiving?

3. Correct the DNS server used by the server.

    It is done in /etc/dnsmasq.resolv.conf. Set it to use [censurfridns.dk](https://blog.censurfridns.dk/dns-servere/)

4. Restart the service by doing `service dnsmasq restart` (as root)

    If it fails, reiterate and fix.

5. Collect your notes on how you tested, concluded and fixed the issues.


## Using the web proxy

1. Read up on what a webproxy is, e.g. [here](https://blog.stackpath.com/proxy-caching/) and [here](https://blog.stackpath.com/proxy-caching/)

    The server is installed with [squid](http://www.squid-cache.org/)

4. Use the server as a webproxy

    Change the configuration in your browser, see [here](https://blazingseollc.com/blog/web-proxies-browsers/) or find a guide specific for your browser.

    `Squid` is running on port tcp/3128 on the server.

5. Is it working?

    Use wireshark to show that you use the server as a proxy and that the proxy forwards the requests.

    Also, there are log files on the server in `/var/log/squid`. Use e.g. `tail -f /var/log/squid/access.log` to mnitor requests.

6. Collect your notes on how you tested, concluded and fixed the issues.

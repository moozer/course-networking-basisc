## Setting up

This first part is about configuring so that a new virtual machine fits in the network.

## Installation

1. Download and install .ova from [here](https://drive.google.com/file/d/1hJVX_yjET5PHl6zNBhmy5SW3R3nNaCWU/view?usp=sharing)

    Username and passwords are `root`:`root123` and `sysuser`:`sysuser123`, ie. username "root" and password "root123"

2. Connect the interface to the NAT network

3. Correct the ip adress

    The box is configured to have a bogus IP address and gateway.

    Go into the server and correct the static IP address to match the vmware network. See e.g. the [debian docs](https://wiki.debian.org/NetworkConfiguration#Configuring_the_interface_manually) for how to do it.

    Notice that `sudo` is not needed (nor installed). It means to log in as superuse, and you have done that already by logging in as `root`.

    Perhaps you need to reboot in order for the changed to come into effect.

4. Test that it is working as intended

    Remember to do screenshots and collect notes.

5. Set up kali on the same network and test that it has internet connection

6. Make a logical network diagram of the system

7. Verify that you have access to the server

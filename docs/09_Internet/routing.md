# Routing

Routing on the internet is the same as on a local network, except it is on a grander scale and the controlling entities, ie. the ASes.

## BGP routing protocol

The protocol used between ASes to announce `paths` is BGP, ie. routing information the network address. An AS has a peering agreement with one or more ASes and will `peer` with them about how to routes packets to and from the AS.

A lot of ASes are "stub", meaning that they only talk with one upstream AS, which simplifies their internal network setup.

An introduction to BGP from [thousandeyes](https://www.thousandeyes.com/resources/how-bgp-works-webinar) and one from [network computing](https://www.networkcomputing.com/data-centers/bgp-basics-internal-and-external-bgp)

Key takaway is that we have different kinds of ASes, and they use BGP for transmitting routing information to each other.

References

* BGP in 4 parts (for reference): [part 1](https://www.kwtrain.com/blog/bgp-pt1), [part 2](https://www.kwtrain.com/blog/bgp-pt2), [part 3](https://www.kwtrain.com/blog/bgp-pt3), [part 4](https://www.kwtrain.com/blog/bgp-pt4)

## Finding the ASes

1. Do a `traceroute -n 8.8.8.8` (or similar)

2. Look up the public ips using `whois <ip>` or do it [online](https://whois.domaintools.com/)

3. Make a list of ASes and company names

4. Do the same for `1.1.1.1`

## Looking glass

Some ASes lets you peek into the routing table of a core router. This is called a looking glass.

There are a lot of these available.

A nice place to go exploring ip ranges, ASes, connected ASes and more is at [RIPEs RIPESTAT](https://beta-ui.stat.ripe.net/launchpad/)

## Testing using RIPESTAT

1. Go to [RIPEs RIPESTAT](https://beta-ui.stat.ripe.net/launchpad/)

2. Search on the first AS from the previous exercise

3. Use the "AS neighbor" panel, and work your way through the AS list.

4. Look at values, company names, number of routes and the other information presented, and explain what is seen.

## Testing looking glasses using telnet

1. Go to [bgp4as](https://www.bgp4.as/looking-glasses)

2. Connect using telnet to one of the servers

    This list is at the bottom of the page, eg. `telnet route-server.he.net`

3. Run `show bgp ipv4 summary`, and explain what is seen

4. Run `show bgp ipv4 unicast`, and explain what is seen

The command should give you an impression of how many AS and route entries a core router has.

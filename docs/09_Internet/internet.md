# Internet

Technically speaking, the internet gives layer 3 connectivity across the globe and using static and dynamic routing is able to handle the dynamic nature of what is available. Access to the internet is usually supplied by [internet service providers](https://ipwithease.com/what-is-isp-internet-service-provider/)

A bit of [history](https://www.livescience.com/20727-internet-history.html) and some [more](https://www.livescience.com/20727-internet-history.html).

The internet is subdivided intop [Autonomous systems](https://www.cloudflare.com/learning/network-layer/what-is-an-autonomous-system/), which are the entities that geet IP addresses from IANA and the RIRs. These entities operate (by owning or renting) the physical connections and "peer" with each other to give access to and from servers and users.

The world-wide-web is the system of interconnected web pages using links. It is using the internet for routing and mostly uses HTTP/S on top.

## From LAN to internet, the ISP

Inside an ordinary LAN, each IP address will appears once, and the network administrators will configure the system to ensure that. This is done using DHCP and static IP according to some network design. Having the wrong IP adress on a subnet, even if it unique, will prevent routers from routing to it, so it will often fail.

In Ipv4, it is the norm to have the external router doing NAT. For this it work, the external IP address must belong to the subnet the router i connected to.  Either this is done using DHCP from the ISP, or setting a static IP, again this is supplied from the ISP.

Some companies have multiple ip adresses, and will allocate some for internet facing servers, and some for user traffic.


## Check the ISP

1. Run `traceroue -n 8.8.8.8`

2. Make a note of the first ip address that is not a private address

3. Look it up using `whois <ip>` or do it [online](https://whois.domaintools.com/)

4. Describe the information you get about the ip.


## Distributing public ip

To handle ip addresses, there is an organisation called [IANA](https://www.iana.org/about), which delegates to the regional internet registries (RIR). There are 5 RIRs, which handles [different geographic regions](https://www.iana.org/numbers).

For the EMEA region [RIPE](https://www.ripe.net/) is the organisation that distributes.

## public IPs

For IPv4 address, the entire address area is allocated.

1. Go [here](https://www.iana.org/assignments/ipv4-address-space/ipv4-address-space.xhtml) to see how the IPv4 addresses are divided.

2. Find all private addresses and note what its "designation" is.

3. Compare the public IP from your ISP, and check if the table matches what you see in the tables.

# The internet

## Goals

The goal for this part is for the student to

* explain the structure of the internet
* explain routing, BGP and AS

There are three parts:

* [Internet](internet.md)
* [routing](routing.md)

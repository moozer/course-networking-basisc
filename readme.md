Networking basics course
====================================

This repo contains course materials for a course in networking.

Local building and testing
-----------------

For local builds and testing

Basic setup
1. `python3 -m virtualenv venv`
2. `source venv/bin/activate`
3. `pip3 install -r requirements.txt`

To build the site: `mkdocs build`

To serve the site locally: `mkdocs serve`

Links checking (after build): `linkchecker docs/ --check-extern -v`

See [.gitlab-ci.yml](.gitlab-ci.yml) for more how it is done in CI.
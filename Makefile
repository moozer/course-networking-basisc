# inspired from: https://venthur.de/2021-03-31-python-makefiles.html

site_files = $(wildcard docs/**/*.md) $(wildcard docs/**/*.png)


# system python interpreter. used only to create virtual environment
PY = python3
VENV = venv
BIN=$(VENV)/bin

$(VENV): requirements.txt
	$(PY) -m venv $(VENV)
	$(BIN)/pip install --upgrade -r requirements.txt
	touch $(VENV)

site: $(VENV) $(site_files) mkdocs.yml
	. venv/bin/activate; mkdocs build


linkcheck.marker:
	touch linkcheck.marker

.PHONY: linkcheck
linkcheck: site linkcheck.marker
	. venv/bin/activate; linkchecker docs/ --check-extern -v
	touch linkcheck.marker

clean:
	rm -r site
	rm -r venv
